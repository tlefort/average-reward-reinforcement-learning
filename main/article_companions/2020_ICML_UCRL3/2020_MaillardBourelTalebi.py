from experiments.fullExperiment import *
from experiments.plotResults import plot_results_from_dump

#######################
# Import registered environments
import environments.MDPs_discrete.registerEnvironments as bW
# To get the list of registered environments:
#print("List of registered environments: ")
#[print(k) for k in bW.registerWorlds.keys()]

# or Import directly some environment

#######################
# Import some learners
import learners.MDPs_discrete.UCRL2 as ucrl2
import learners.MDPs_discrete.UCRL2B as ucrl2b
import learners.MDPs_discrete.KLUCRL as klucrl
import learners.MDPs_discrete.PSRL as psrl
import learners.MDPs_discrete.UCRL3 as ucrl3


# TODO: Adapt to novel version of API

#######################
# Instantiate one environment
#env = bW.makeWorld(bW.registerWorld('grid-2-room'))
#env = bW.makeWorld(bW.registerWorld('grid-4-room'))
env = bW.makeWorld(bW.register_MDPs_discrete('river-swim-6'))
#env = bW.makeWorld(bW.registerWorld('grid-random'))

#######################
# Instantiate a few learners to be compared:
agents = []
nS = env.observation_space.n
nA = env.action_space.n
delta=0.05
agents.append( [ucrl2.UCRL2,{"nS":nS, "nA":nA, "delta":delta}])
agents.append( [ucrl2b.UCRL2B,{"nS":nS, "nA":nA, "delta":delta}])
agents.append( [klucrl.KLUCRL,{"nS":nS, "nA":nA, "delta":delta}])
agents.append( [psrl.PSRL,{"nS":nS, "nA":nA, "delta":delta}])
agents.append( [ucrl3.UCRL3_lazy, {"nS":nS, "nA":nA, "delta":delta}])

#############################
# Compute oracle policy:
oracle = opt.build_opti(env.name, env.env, env.observation_space.n, env.action_space.n)

#######################
# Run a full experiment
#######################
import os
from experiments.utils import get_project_root_dir
ROOT= get_project_root_dir()+"/main/templates/"+"generictemplate"+"_results/"
os.mkdir(ROOT)

runLargeMulticoreExperiment(env, agents, oracle, timeHorizon=5000, nbReplicates=16,root_folder=ROOT)

#######################

# Below is past approach from article
#######################
# Running a full example on a very large horizon:
#######################
# run_large_exp('riverSwim6', timeHorizon=1000000, nbReplicates=50)
# run_large_exp('riverSwim25', timeHorizon=10000000, nbReplicates=50)
# run_large_exp('2-room', timeHorizon=10000000, nbReplicates=50)
# run_large_exp('4-room', timeHorizon=10000000, nbReplicates=50)
# run_large_exp('random100', timeHorizon=10000000, nbReplicates=50)
# run_large_exp('randomRich', timeHorizon=10000000, nbReplicates=50)


#######################
# Assuming data has been generated in previous runs, dumped in results/ in following files:
#cumRegret_Gridworld-4-room-v0_UCRL2_10000000
#cumRegret_Gridworld-4-room-v0_KL-UCRL_10000000
#cumRegret_Gridworld-4-room-v0_UCRL2B_10000000
#cumRegret_Gridworld-4-room-v0_PSRL_10000000
#cumRegret_Gridworld-4-room-v0_UCRL3_10000000
#Choose tplot<=tmax=10000000
#######################
def plotXpsArticle():
    tmax=10000000
    plot_results_from_dump('riverSwim6',tplot=tmax/10)
    plot_results_from_dump('4-room',tplot=tmax)
    plot_results_from_dump('2-room',tplot=tmax)
    plot_results_from_dump('random100',tplot=tmax)
    plot_results_from_dump('randomRich',tplot=tmax)
    plot_results_from_dump('riverSwim25',tplot=tmax)

#plotXpsArticle()