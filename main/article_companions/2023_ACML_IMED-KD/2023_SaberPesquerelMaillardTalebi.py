
'''
    Companion code for the article
    "Logarithmic regret in communicating MDPs: Leveraging known dynamics with bandits"
    H. Saber, F. Pesquerel, O-A. Maillard, M. S. Talebi
    Asian Conference of Machine Learning, 2023.
'''
# TODO: Adapt it to reproduce Xps from the paper




from experiments.fullExperiment import *

import environments.MDPs_discrete.registerEnvironments as bW
import learners.Generic.Qlearning as ql
import learners.MDPs_discrete.KnownDynamics.UCRL3_KD as ucrl_KD
import learners.MDPs_discrete.KnownDynamics.PSRL_KD as psrl_KD
import learners.MDPs_discrete.KnownDynamics.IMEDKTP as imedktp
import learners.MDPs_discrete.KnownDynamics.UCB_KD as ucb_KD
import learners.MDPs_discrete.KnownDynamics.TS_KD as ts_KD

#import learners.MDPs_discrete.IMEDKD as iKD

# To get the list of registered environments:
#print("List of registered environments: ")
#[print(k) for k in bW.registerWorlds.keys()]

env = bW.makeWorld(bW.register_MDPs_discrete('ergo-river-swim-6'))
# env = bW.makeWorld(bW.register_MDPs_discrete('grid-2-room'))
# env = bW.makeWorld(bW.register_MDPs_discrete('grid-4-room'))
# env = bW.makeWorld(bW.register_MDPs_discrete('ergo-river-swim-25'))
# env = bW.makeWorld(bW.register_MDPs_discrete('nasty'))


agents = []
nS = env.observation_space.n
nA = env.action_space.n
delta=0.05
#agents.append( [random.Random, {"env": env.env}])
agents.append( [ql.Qlearning, {"nS":nS, "nA":nA}])
agents.append([imedktp.IMEDKTP, {"env": env}])  # IMED-KD
agents.append([ucb_KD.UCBKD, {"env": env}])  # UCB-KD
# agents.append( [UCRL3KTP.UCRL3_lazy, {"nS":nS, "nA":nA, "env":env, "delta":delta}])  # UCRL3
# agents.append( [PSRLKTP.PSRLKTP, {"nS":nS, "nA":nA, "env":env, "delta":delta}])  # PSRL
# agents.append([ql.Qlearning, {"nS": nS, "nA": nA}])  # Q-learning
agents.append([ts_KD.TSKD, {"env": env}])  # TS-KD
#agents.append( [ql.AdaQlearning, {"nS":nS, "nA":nA}])
#agents.append( [ucrl.UCRL2, {"nS":nS, "nA":nA, "delta":delta}])
#agents.append( [ucrlb.UCRL2B, {"nS":nS, "nA":nA, "delta":delta}])
#agents.append( [klucrl.KL_UCRL, {"nS":nS, "nA":nA, "delta":delta}])
#agents.append( [ucrl3.UCRL3_lazy, {"nS":nS, "nA":nA, "delta":delta}])

#agents.append([ucrl_KD.UCRL3_lazy, {"nS":nS, "nA":nA, "env": env.env, "delta":delta}])
#agents.append([psrl_KD.PSRL_KD, {"nS":nS, "nA":nA, "env": env.env, "delta":delta}])
#agents.append( [imed_KD.IMEDKTP, {"nS":nS, "nA":nA, "env": env.env}])
#agents.append([imed_KD.IMED_KD, {"env": env.env}])



#############################
# Compute oracle policy:
oracle = opt.build_opti(env.name, env.env, env.observation_space.n, env.action_space.n)


#######################
# Run a full experiment
#######################
import os
from experiments.utils import get_project_root_dir
ROOT= get_project_root_dir()+"/main/article_companions/2023_ACML_IMED-KD/"+"2023_SaberPesquerelMaillardTalebi"+"_results/"
os.mkdir(ROOT)

runLargeMulticoreExperiment(env, agents, oracle, timeHorizon=20000, nbReplicates=32,root_folder=ROOT)

#######################
# Plotting Regret directly from dump files of past runs:
#files = plR.search_dump_cumRegretfiles("RiverSwim-6-v0", root_folder=ROOT)
#plR.plot_results_from_dump(files, 500)
